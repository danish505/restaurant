<?php include('header.php');?>

<?php 
require 'includes/db.php';
$data = get_orders();
print_r($data);
?>


                    <div class="col-md-12">
                        <div class="c-table-responsive@wide">
                            <table class="c-table">
                                <caption class="c-table__title">
                                    Invoices
                                </caption>
                                <thead class="c-table__head c-table__head--slim">
                                    <tr class="c-table__row">
                                        <th class="c-table__cell c-table__cell--head">No.</th>
                                        <th class="c-table__cell c-table__cell--head">Invoice Subject</th>
                                        <th class="c-table__cell c-table__cell--head">Client</th>
                                        <th class="c-table__cell c-table__cell--head">VAT No.</th>
                                        <th class="c-table__cell c-table__cell--head">Created</th>
                                        <th class="c-table__cell c-table__cell--head">Status</th>
                                        <th class="c-table__cell c-table__cell--head">Price</th>
                                        <th class="c-table__cell c-table__cell--head">
                                            <span class="u-hidden-visually">Actions</span>
                                        </th>
                                        <th class="c-table__cell c-table__cell--head">
                                            <span class="u-hidden-visually">Download</span>
                                        </th>
                                    </tr>
                                </thead>

                                <tbody>
                                    <tr class="c-table__row">
                                        <td class="c-table__cell"><span class="u-text-mute">00450</span></td>
                                        <td class="c-table__cell">Design Works</td>
                                        <td class="c-table__cell">
                                            <span class="u-text-mute">Carlson Limited</span>
                                        </td>
                                        <td class="c-table__cell">
                                            <span class="u-text-mute">87956621</span>
                                        </td>
                                        <td class="c-table__cell">
                                            <span class="u-text-mute">15 Dec 2017</span>
                                        </td>
                                        <td class="c-table__cell">
                                            <span class="c-badge c-badge--small c-badge--success">Paid</span>
                                        </td>
                                        <td class="c-table__cell">$887</td>

                                        <td class="c-table__cell u-text-right">
                                            <a href="invoice.html" class="c-btn c-btn--info u-mr-xsmall">Manage</a>
                                            <div class="c-dropdown dropdown">
                                                <button class="c-btn c-btn--secondary has-dropdown dropdown-toggle" id="dropdownMenuButton10" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Actions</button>
                                                
                                                <div class="c-dropdown__menu dropdown-menu" aria-labelledby="dropdownMenuButton10">
                                                    <a class="c-dropdown__item dropdown-item" href="#">Complete</a>
                                                    <a class="c-dropdown__item dropdown-item" href="#">Share</a>
                                                    <a class="c-dropdown__item dropdown-item" href="#">Archive</a>
                                                </div>
                                            </div>
                                        </td>
                                        <td class="c-table__cell">
                                            <a class="u-text-mute" href="invoice.html">
                                                <i class="fa fa-cloud-download"></i>
                                            </a>
                                        </td>
                                    </tr><!-- // .table__row -->

                                      
                                </tbody>
                            </table>
                        </div><!-- // .c-card -->
                    </div>
                
<?php include('footer.php'); ?>