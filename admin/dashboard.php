<?php include('header.php'); ?>


<div class="container-fluid">
                <div class="row">
                    <div class="col-xl-4">
                        <div class="c-graph-card" data-mh="graph-cards">
                            <div class="c-graph-card__content">
                                <h3 class="c-graph-card__title">Total Earnings</h3>
                                <p class="c-graph-card__date">In Year</p>
                                <h4 class="c-graph-card__number">Rs. 2,190</h4>
                            </div>
                            <div class="c-graph-card__chart">
                                <canvas id="js-chart-payout" width="300" height="74"></canvas>
                            </div>
                        </div>
                    </div>

                    <div class="col-xl-4">
                        <div class="c-graph-card" data-mh="graph-cards">
                            <div class="c-graph-card__content">
                                <h3 class="c-graph-card__title">Total Earnings</h3>
                                <p class="c-graph-card__date">In Months</p>
                                <h4 class="c-graph-card__number">Rs. 23,580</h4>
                            </div>
                            <div class="c-graph-card__chart">
                                <canvas id="js-chart-earnings" width="300" height="74"></canvas>
                            </div>
                        </div>
                    </div>                  
                </div>

               

                <div class="row">
                        <div class="col-md-12">
                                <div class="cart-table c-table-responsive@tablet">
                                    <table class="c-table">
                                        <thead class="c-table__head">
                                            <tr class="c-table__row">
                                                <th class="c-table__cell c-table__cell--head">SELECT MONTH TO</th>
                                                <th class="c-table__cell c-table__cell--head">SELECT MONTH FROM</th>
                                                <th class="c-table__cell c-table__cell--head">YEAR</th>
                                                <th class="c-table__cell c-table__cell--head">ACTION</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr class="c-table__row">
                                                
                                                <td class="c-table__cell">
                                                    <!-- Select with Search Enabled Select -->
                                                    <div class="c-field u-mb-medium">
                                                        <label class="c-field__label" for="select1">Single Select (Search Enabled)</label>
                                                        <!-- Select2 jquery plugin is used -->
                                                        <select class="c-select has-search" id="select1">
                                                            <option>January</option>
                                                            <option>February</option>
                                                            <option>March</option>
                                                            <option>April</option>
                                                            <option>May</option>
                                                            <option>June</option>
                                                            <option>July</option>
                                                            <option>August</option>
                                                            <option>September</option>
                                                            <option>October</option>
                                                            <option>November</option>
                                                            <option>December</option>
                                                        </select>
                                                    </div>
                                                </td>
                                                <td class="c-table__cell">
                                                        <div class="c-field u-mb-medium">
                                                            <label class="c-field__label" for="select2">Single Select (Search Enabled)</label>
                                                            <!-- Select2 jquery plugin is used -->
                                                            <select class="c-select has-search" id="select2">
                                                                <option>January</option>
                                                                <option>February</option>
                                                                <option>March</option>
                                                                <option>April</option>
                                                                <option>May</option>
                                                                <option>June</option>
                                                                <option>July</option>
                                                                <option>August</option>
                                                                <option>September</option>
                                                                <option>October</option>
                                                                <option>November</option>
                                                                <option>December</option>
                                                            </select>
                                                        </div>
                                                </td>
                                                <td class="c-table__cell">
                                                    <div class="item-price">
                                                    
                                                            <div class="c-field u-mb-medium">
                                                                    <label class="c-field__label" for="select3">Single Select (Search Enabled)</label>
                                                                    <!-- Select2 jquery plugin is used -->
                                                                    <select class="c-select has-search" id="select3">
                                                                        <option>2000</option>
                                                                        <option>2001</option>
                                                                        <option>2002</option>
                                                                        <option>2003</option>
                                                                        <option>2004</option>
                                                                        <option>2005</option>
                                                                        <option>2006</option>
                                                                        <option>2007</option>
                                                                        <option>2008</option>
                                                                        <option>2009</option>
                                                                        <option>2010</option>
                                                                        <option>2011</option>
                                                                        <option>2012</option>
                                                                        <option>2013</option>
                                                                        <option>2014</option>
                                                                        <option>2015</option>
                                                                        <option>2016</option>
                                                                        <option>2017</option>
                                                                        <option>2018</option>
                                                                    </select>
                                                                </div>

                                                    </div>
                                                </td>
                                                <td class="c-table__cell">
                                                   
                                                    <div class="action-btn">
                                                        <a class="c-btn c-btn--danger u-text-xsmall" href="#"> <span class="fa fa-file-pdf-o u-mr-xsmall"></span> Generate PDF</a>
                                                    </div>
                                                </td>
                
                                            </tr>
                                           
                                          
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                </div><!-- // .row -->

     

            </div><!-- // .container -->
            
        </main>
<?php include('footer.php'); ?>