<!doctype html>
<html lang="en-us">

<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Restaurant Automation System</title>
    <!-- Google Font -->
    <link href="https://fonts.googleapis.com/css?family=Montserrat:100,200,300,400,500,600,700,800,900" rel="stylesheet">
    <!-- Favicon -->
    <!-- Stylesheet -->
    <link rel="stylesheet" href="css/main.min.css">
    <link rel="stylesheet" href="css/style.css">

</head>

<body>

    <section class="top-header">
        <div class="container">
            <div class="row">
                <header class="c-navbar nav-header">
                    <a class="c-navbar__brand" href="#!">
                        <h5>Restaurant Automation System</h5>
                    </a>
                    <!-- Navigation items that will be collapes and toggle in small viewports -->
                    <nav class="c-nav collapse" id="main-nav">
                        <ul class="c-nav__list">
                            <li class="c-nav__item">
                                <a class="c-btn c-btn--danger u-text-xsmall" href="index.php">BACK</a>
                            </li>
                        </ul>
                    </nav>
                    <!-- // Navigation items  -->
                    <button class="c-nav-toggle" type="button" data-toggle="collapse" data-target="#main-nav">
                        <span class="c-nav-toggle__bar"></span>
                        <span class="c-nav-toggle__bar"></span>
                        <span class="c-nav-toggle__bar"></span>
                    </button>
                </header>
            </div>
        </div>
    </section>

    <section class="cart-section">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="cart-heading">
                        <h2>YOUR SHOPPING CART</h2>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="cart-table c-table-responsive@tablet">
                        <table class="c-table">
                            <thead class="c-table__head">
                                <tr class="c-table__row">
                                    <th class="c-table__cell c-table__cell--head">QUANTITY</th>
                                    <th class="c-table__cell c-table__cell--head">ITEM NAME</th>
                                    <th class="c-table__cell c-table__cell--head">PRICE</th>
                                     
                                </tr>
                            </thead>
                            <tbody>
                                <tr class="c-table__row">
                                    <td class="c-table__cell">
                                        <div class="c-field">
                                            <input class="c-input" id="input1" type="number" min="0" placeholder="QTY">
                                        </div>
                                    </td>
                                    <td class="c-table__cell">
                                        <div class="item-name">
                                            <h5>PEPPERONI PIZZA</h5>
                                        </div>
                                    </td>
                                    <td class="c-table__cell">
                                        <div class="item-price">
                                            <h6><span>Rs.</span> 1000</h6>
                                        </div>
                                    </td>
                                     
                                </tr>
                               
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="row action-row">
                <div class="col-md-10">
                    <div class="total-price">
                        <h6>TOTAL: <span> <label for="">Rs.</label> </span></h6>
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="btn-order">
                            <a class="c-btn c-btn--success u-width-100" href="#">PLACE ORDER</a>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <footer>
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="copy-right-text">
                        <p>Copyright © 2018 Restaurant Automation System. All rights reserved.</p>
                    </div>
                </div>
            </div>
        </div>
    </footer>

    <script src="js/main.min.js"></script>
   
</body>

</html>