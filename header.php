<!doctype html>
<html lang="en-us">

<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Restaurant Automation System</title>
    <!-- Google Font -->
    <link href="https://fonts.googleapis.com/css?family=Montserrat:100,200,300,400,500,600,700,800,900" rel="stylesheet">
    <!-- Favicon -->
    <!-- Stylesheet -->
    <link rel="stylesheet" href="css/main.min.css">
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="css/owl.carousel.min.css">
    <link rel="stylesheet" href="css/owl.theme.default.min.css">
</head>

<body>

    <section class="top-header">
        <div class="container">
            <div class="row">
                <header class="c-navbar nav-header">
                    <a class="c-navbar__brand" href="#!">
                        <h5>Restaurant Automation System</h5>
                    </a>
                    <!-- Navigation items that will be collapes and toggle in small viewports -->
                    <nav class="c-nav collapse" id="main-nav">
                        <ul class="c-nav__list">
                           <li class="c-nav__item cart">
                                 <div class="c-dropdown dropdown">
                    <a  class="c-avatar c-avatar--xsmall has-dropdown dropdown-toggle" href="#" id="dropdwonMenuAvatar" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                       <i class="fa fa-shopping-cart"></i>
                    </a>

                 <div class="c-dropdown__menu dropdown-menu dropdown-menu-right" aria-labelledby="dropdwonMenuAvatar">

                        <a class="c-dropdown__item dropdown-item" href="#">Place Order</a>
                    </div>
                </div>
                            </li>
                        </ul>
                    </nav>
                    <!-- // Navigation items  -->
        <button class="c-nav-toggle" type="button" data-toggle="collapse" data-target="#main-nav">
                        <span class="c-nav-toggle__bar"></span>
                        <span class="c-nav-toggle__bar"></span>
                        <span class="c-nav-toggle__bar"></span>
                    </button>
                </header>
            </div>
        </div>
    </section>
